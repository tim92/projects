$(document).ready(function() {

  const button = $('.pop');
  const animatedElements = [
    {
      'el': $('.circle'),
      'class': 'circle_animate'
    },
    {
      'el': $('.banner'),
      'class': 'banner-animate'
    },
    {
      'el': $('.achieve_disp'),
      'class': 'achieve_disp_animate'
    }
  ];

  function animateElements() {
    animatedElements.forEach(element => {
      element['el'].addClass(element['class']);
      setTimeout(() => {
        element['el'].removeClass(element['class']);
      }, 10000)
    });
  }

  function placeValues() {
    var nameInput = 'Beziehung';
    $('.achiev_name').val(nameInput);
    var scoreInput = '1 Jahr';
    $('.acheive_score').val(scoreInput);
  }

  button.on('click', function() {
    placeValues();
    animateElements();
    $(this).addClass('animating');
    setTimeout(() => {
      $(this).removeClass('animating');
    }, 1000);
  });
  
  $('.input').on('focus', function(){
    $(this).parent().addClass('input-focussed')
  })
  $('.input').on('blur', function(){
    var thisInput = $(this);
    $(this).parent().removeClass('input-focussed');
    if(thisInput.val() != ""){
      thisInput.parent().addClass('input-complete');
    } else {
      thisInput.parent().removeClass('input-complete');
    }
  })
});
