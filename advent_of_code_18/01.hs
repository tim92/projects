{-# OPTIONS_GHC -Wall #-}

main :: IO ()
main = interact (show . sum . map (read . filter (/= '+')) . lines)
