{ nixpkgs ? import <nixpkgs> {} }:
let
  inherit (nixpkgs) pkgs;
  inherit (pkgs) haskellPackages;

  haskellDeps = ps: with ps; [
  ];

  ghc = haskellPackages.ghcWithPackages haskellDeps;

  nixPackages = [
    ghc
    haskellPackages.cabal-install
    haskellPackages.hlint
  ];
in
pkgs.stdenv.mkDerivation {
  name = "advent18";
  buildInputs = nixPackages;
}
