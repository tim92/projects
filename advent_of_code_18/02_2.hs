{-# OPTIONS_GHC -Wall #-}

import Data.Char
import Data.List

main :: IO ()
main = do
    box_id_list <- fmap lines getContents
    let checksum_list = map checksum box_id_list
    let sorted_pairs = sortBy (\(_,a) (_,b) -> compare a b) $ zip box_id_list checksum_list
    print $ idLetters sorted_pairs !! 0
-- TODO: remove character that differs for correct puzzle solution


-- compare strings and return all but different char where only one char differs
idLetters :: [(String, Int)] -> [String]
idLetters [] = []
idLetters (x:xs)
    | length sim_strings > 1 = intersections:(idLetters xs)
    | otherwise = (idLetters xs)
    where
        sim_strings = (similarStrings x xs)
        intersections = intersect (head sim_strings) (head (tail sim_strings))

-- similar strings based on checksum and intersection of the string
similarStrings :: (String, Int) -> [(String, Int)] -> [String]
similarStrings x [] = (fst x):[]
similarStrings x xs
    | (checksum_diff < 27) && intersection = (fst (head xs)):(similarStrings x (tail xs))
    | otherwise = similarStrings x (tail xs)
    where
        checksum_diff = (abs ((snd x) - (snd (head xs))))
        intersection = (length (intersect' (fst x) (fst (head xs)))) == ((length (fst x)) - 1)

-- "real" intersection of strings: intersect' "abcc" "adcc" => "ac"
intersect' :: String -> String -> String
intersect' s1 s2 = [fst x | x <- (zip s1 s2), fst x == snd x]

-- use alphanumerical sum as checksum
checksum :: String -> Int
checksum s = sum $ map ord s
