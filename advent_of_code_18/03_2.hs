import qualified Data.Set as S

-- 686 no idea whats wrong..

main :: IO ()
main = do
    let sidelength = 1000
    areaDescriptions <- fmap lines getContents
    let coordinates = map (calcNumbers sidelength) (map parse areaDescriptions)
    print $ findIntact coordinates 0 S.empty S.empty


-- representation of index, coordinates and spanning area
data Coord = Coord Int Int Int Int Int deriving (Show)

-- calculate numbers representing positions in area with given sidelength
calcNumbers :: Int -> Coord -> (Int, [Int])
calcNumbers sidelength (Coord i startX startY lenX lenY) =
    (i, [(y - 1) * sidelength + x |
        x <- [(startX + 1)..(startX + lenX)],
        y <- [(startY + 1)..(startY + lenY)]])

-- keep set of overlaps, return index of intact element
findIntact :: [(Int, [Int])] -> Int -> S.Set Int -> S.Set Int -> Int
findIntact [] res _ _ = res
findIntact xss previ occupied duplicate =
    findIntact t i occ dupl
    where
        h = head xss
        t = tail xss
        addset = S.fromList (snd h)
        occ = S.union occupied addset
        before = S.size duplicate
        dupl = S.union duplicate (S.intersection occupied addset)
        after = S.size dupl
        i = (if before == after then fst h else previ)

-- parsing function, turns "#123 @ 3,2: 5x4" into Coord
parse :: String -> Coord
parse s = Coord i startX startY lenX lenY
    where
        i = read (takeWhile (/= ' ') (drop 1 s))
        numbers = drop 2 (dropWhile (/= '@') s)
        starts = takeWhile (/= ':') numbers
        ranges = drop 2 (dropWhile (/= ':') numbers)
        startX = read (takeWhile (/= ',') starts)
        startY = read (tail (dropWhile (/= ',') starts))
        lenX = read (takeWhile (/= 'x') ranges)
        lenY = read (tail (dropWhile (/= 'x') ranges))
