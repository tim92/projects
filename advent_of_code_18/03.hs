import qualified Data.Set as S


main :: IO ()
main = do
    let sidelength = 1000
    area_descriptions <- fmap lines getContents
    let coordinates = map (calcNumbers sidelength) (map parse area_descriptions)
    print $ countDuplicates coordinates S.empty S.empty


-- representation of coordinates and spanning area
data Coord = Coord Int Int Int Int deriving (Show)

-- calculate numbers representing positions in area with given sidelength
calcNumbers :: Int -> Coord -> [Int]
calcNumbers sidelength (Coord startX startY lenX lenY) =
    [(y - 1) * sidelength + x |
        x <- [(startX + 1)..(startX + lenX)],
        y <- [(startY + 1)..(startY + lenY)]]

-- keep set of overlaps, return length
countDuplicates :: [[Int]] -> S.Set Int -> S.Set Int -> Int
countDuplicates [] _ duplicate = S.size duplicate
countDuplicates xss occupied duplicate =
  countDuplicates t occ dupl
  where
    h = head xss
    t = tail xss
    occ = S.union occupied (S.fromList h)
    dupl = S.union duplicate (S.intersection occupied (S.fromList h))

-- parsing function, turns "#123 @ 3,2: 5x4" into Coord
parse :: String -> Coord
parse s = Coord startX startY lenX lenY
    where
        numbers = drop 2 (dropWhile (/= '@') s)
        starts = takeWhile (/= ':') numbers
        ranges = drop 2 (dropWhile (/= ':') numbers)
        startX = read (takeWhile (/= ',') starts)
        startY = read (tail (dropWhile (/= ',') starts))
        lenX = read (takeWhile (/= 'x') ranges)
        lenY = read (tail (dropWhile (/= 'x') ranges))

