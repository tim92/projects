{-# OPTIONS_GHC -Wall #-}

main :: IO ()
main = do
    box_id_list <- fmap lines getContents
    print $ wrap trippleLetters box_id_list * wrap doubleLetters box_id_list


-- checks if a string contains any character exactly given times
multLetters :: String -> Int -> Bool
multLetters [] _   = False
multLetters s1 num =
  num == length (filter (== head s1) s1) ||
  multLetters (filter (/= head s1) (tail s1)) num

-- whether a string contains any character exactly three times
trippleLetters :: String -> Bool
trippleLetters s1 = multLetters s1 3

-- whether a string contains any character exactly two times
doubleLetters :: String -> Bool
doubleLetters s1 = multLetters s1 2

-- count number of double/tripple letters in list of strings
wrap :: (String -> Bool) -> [String] -> Int
wrap fun xs = length (filter (== True) $ map fun xs)

