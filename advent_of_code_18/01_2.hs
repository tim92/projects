{-# OPTIONS_GHC -Wall #-}

main :: IO ()
main = do
    freq_list <- fmap (map ((read :: String -> Int) . filter (/= '+')) . lines) getContents
    print $ findSecondOccurring [] $ 0:(addListElements 0 (cycle freq_list))


addListElements :: Int -> [Int] -> [Int]
addListElements _ [] = []
addListElements x xs = xplus:(addListElements xplus (tail xs))
    where xplus = x + head xs

findSecondOccurring :: [Int] -> [Int] -> Int
findSecondOccurring xs1 xs2
    | elem head_xs2 xs1 = head_xs2
    | otherwise = findSecondOccurring (head_xs2:xs1) (tail xs2)
    where head_xs2 = head xs2

