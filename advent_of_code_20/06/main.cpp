#include <algorithm>
#include <fstream>
#include <bitset>
#include <iostream>
#include <string>
#include <vector>

using namespace std;


int main() {
  cout << "\n";
  string line;
  vector<string> group;
  ifstream f("input");
  vector<vector<string>> forms;

  while (getline(f, line)) {
    if (line.length() > 0) {
      group.push_back(line);
    } else {
      forms.push_back(group);
      group.clear();
    }
  }
  forms.push_back(group);

  int valid = 0;
  int valid2 = 0;
  for (auto group : forms) {
    // part one
    string groupline;
    for (auto line : group)
      groupline += line;
    while (groupline.length() > 0) {
      char c = groupline.at(0);
      if (string::npos != groupline.find(c)) {
        groupline.erase(remove(groupline.begin(), groupline.end(), c), groupline.end());
        valid++;
      }
    }

    // part two
    string first = group.at(0);
    // only need to check characters in first person string
    for (auto c : first) {
      size_t groupanswer = 0;
      for (auto person : group) {
        if (string::npos != person.find(c)) {
          groupanswer++;
        }
      }
      if (groupanswer == group.size())
        valid2++;
    }
  }

  cout << "Part One: " << valid << "\n";
  cout << "Part Two: " << valid2 << "\n";

  cout << "\n";
}
