#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;


int main(int argc, char* argv[]) {
    string tmp;
    ifstream f("input");
    int len = 1000;
    int min[len], max[len];
    char c[len];
    string pw[len];
    int index = 0;

    while (getline(f, tmp)) {
        int i0 = tmp.find('-');
        min[index] = stoi(tmp.substr(0, i0));
        int i1 = tmp.find(' ');
        max[index] = stoi(tmp.substr(i0 + 1, i1));
        int i2 = tmp.find(':');
        c[index] = tmp.substr(i1 + 1, i2)[0];
        pw[index] = tmp.substr(i2 + 2);
        index++;
    }

    int valid = 0;
    int valid2 = 0;
    for (int x = 0; x < len; x++) {
        // Part One
        int occurences = count(pw[x].begin(), pw[x].end(), c[x]);
        if (occurences >= min[x] && occurences <= max[x])
            valid++;

        // Part Two
        char c1 = pw[x][min[x] - 1];
        char c2 = pw[x][max[x] - 1];
        if ((c1 == c[x] && c2 != c[x]) || (c1 != c[x] && c2 == c[x]))
            valid2++;
    }
    cout << "Part One: " << valid << "\n";
    cout << "Part Two: " << valid2 << "\n";

    cout << "\n";
}
