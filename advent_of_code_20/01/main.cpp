#include <string>
#include <iostream>
#include <fstream>

using namespace std;


int main(int argc, char* argv[]) {
    string tmp;
    ifstream f("input");
    int len = 200;
    int num[len];
    int index = 0;

    while (getline(f, tmp)) {
        num[index] = stoi(tmp);
        index++;
    }

    for (int x = 0; x < len; x++) {
        for (int y = x; y < len; y++) {
            if (num[x] + num[y] == 2020)
                cout << "Part One: " << num[x] << " * " << num[y] << " = " << num[x] * num[y] << "\n";
            for (int z = y; z < len; z++) {
                if (num[x] + num[y] + num[z] == 2020)
                    cout << "Part Two: " << num[x] << " * " << num[y] << " * " << num[z] << " = " << num[x] * num[y] * num[z] << "\n";
            }
        }
    }

    cout << "\n";
}
