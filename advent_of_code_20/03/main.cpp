#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;


int main(int argc, char* argv[]) {
    string tmp;
    ifstream f("input");
    int len = 323;
    bool map[len][31];
    int index = 0;

    while (getline(f, tmp)) {
        for (int i = 0; i < tmp.length(); i++) {
            if (tmp[i] == '.')
                map[index][i] = false;
            else
                map[index][i] = true;
        }
        index++;
    }

    int rows[5] = { 0 };
    int cols[5] = { 0 };
    int rowpluses[5] = { 1, 1, 1, 1, 2 };
    int colpluses[5] = { 1, 3, 5, 7, 1 };
    int trees[5] = { 0 };
    long int res = 1;

    for (int r = 0; r < len; r++) {
        for (int i = 0; i < 5; i++) {
            if (map[rows[i]][cols[i]] == 1 && rows[i] < len)
                trees[i]++;
            rows[i] += rowpluses[i];
            cols[i] += colpluses[i];
            if (cols[i] >= 31)
                cols[i] = cols[i] % 31;
        }
    }
    for (int i = 0; i < 5; i++) {
        res *= trees[i];
    }
    cout << "Part One: " << trees[1] << "\n";
    cout << "Part Two: " << trees[0] << " * " << trees[1] << " * " << trees[2] << " * " << trees[3] << " * " << trees[4] << " = " << res << "\n";

    cout << "\n";
}
