#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <vector>

using namespace std;


int main(int argc, char* argv[]) {
    string tmp;
    ifstream f("input");
    vector<map<string, string>> passports;
    int index = 0;
    map<string, string> entry;

    while (getline(f, tmp)) {
        if (tmp.length() > 0) {
            stringstream ss(tmp);
            string pair;
            while (ss >> pair) {
                int i = pair.find(':');
                entry.insert({pair.substr(0, i), pair.substr(i + 1)});
            }
        } else {
            passports.push_back(entry);
            entry.clear();
            index++;
        }
    }
    passports.push_back(entry);
    index++;

    int valid = 0;
    int valid2 = 0;
    string karr[] = { "byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid" };
    vector<string> keys (karr, karr + sizeof(karr) / sizeof(string));
    string eyearr[] = { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" };
    vector<string> eye (eyearr, eyearr + sizeof(eyearr) / sizeof(string));

    for (int i = 0; i < index; i++) {
        int ctr = 0;
        int ctr2 = 0;
        map<string, string> pp = passports[i];
        for (int j = 0; j < keys.size(); j++) {
            string k = keys[j];
            if (pp.count(k)) {
                ctr++;
                if (k == "byr") {
                    if (pp.at(k).size() == 4) {
                        int year = stoi(pp.at(k));
                        if (year >= 1920 && year <= 2002)
                            ctr2++;
                    }
                } else if (k == "ecl") {
                    if (pp.at(k).size() == 3) {
                        for (int l = 0; l < eye.size(); l++) {
                            if (eye[l] == pp.at(k))
                                ctr2++;
                        }
                    }
                } else if (k == "eyr") {
                    if (pp.at(k).size() == 4) {
                        int year = stoi(pp.at(k));
                        if (year >= 2020 && year <= 2030)
                            ctr2++;
                    }
                } else if (k == "hcl") {
                    string c = pp.at(k);
                    if (c.length() == 7 && c[0] == '#') {
                        int correct = 0;
                        for (int l = 1; l < 7; l++) {
                            // 0:48, 9:57, a:97, f:102
                            if ((c[l] >= 48 && c[l] <= 57) || (c[l] >= 97 && c[l] <= 102))
                                correct++;
                        }
                        if (correct == 6)
                            ctr2++;
                    }
                } else if (k == "hgt") {
                    string h = pp.at(k);
                    if (h.find("in") < h.length()) {
                        int in = stoi(h.substr(0, h.find("in")));
                        if (in >= 59 && in <= 76)
                            ctr2++;
                    }
                    if (h.find("cm") < h.length()) {
                        int cm = stoi(h.substr(0, h.find("cm")));
                        if (cm >= 150 && cm <= 193)
                            ctr2++;
                    }
                } else if (k == "iyr") {
                    if (pp.at(k).size() == 4) {
                        int year = stoi(pp.at(k));
                        if (year >= 2010 && year <= 2020)
                            ctr2++;
                    }
                } else if (k == "pid") {
                    string c = pp.at(k);
                    if (c.length() == 9) {
                        int correct = 0;
                        for (int l = 0; l < 9; l++) {
                            // 0:48, 9:57
                            if (c[l] >= 48 && c[l] <= 57)
                                correct++;
                        }
                        if (correct == 9)
                            ctr2++;
                    }
                }
            }
        }
        if (ctr == keys.size())
            valid++;
        if (ctr2 == keys.size()) {
            valid2++;
            cout << "VALID ";
        } else {
            cout << "INVAL ";
        }
        for (auto& entry: pp) {
            cout << entry.first << ":" << entry.second << ", ";
        }
        cout << "\n";
    }
    cout << "Part One: " << valid << "\n";
    cout << "Part Two: " << valid2 << "\n";

    cout << "\n";
}
