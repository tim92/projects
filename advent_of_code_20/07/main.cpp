#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>

using namespace std;


bool checkContain(string check, string bag, map<string, map<string, int>> baglist) {
  bool b1, b2 = false;
  b1 = baglist.at(bag).find(check) != baglist.at(bag).end();
  if (!baglist.at(bag).empty()) {
    for (auto subbag : baglist.at(bag))
      if (checkContain(check, subbag.first, baglist))
        b2 = true;
  }
  return b1 || b2;
}

int sumContained(string bag, map<string, map<string, int>> baglist) {
  bool b1, b2 = false;
  b1 = baglist.at(bag).find(check) != baglist.at(bag).end();
  if (!baglist.at(bag).empty()) {
    for (auto subbag : baglist.at(bag))
      if (checkContain(check, subbag.first, baglist))
        b2 = true;
  }
  return b1 || b2;
}

int main() {
  cout << endl;
  string line;
  ifstream f("input");
  map<string, map<string, int>> baglist;
  map<string, int> children;

  while (getline(f, line)) {
    int keysplit = line.find("bags contain");
    string key = line.substr(0, keysplit - 1); // initial bag name
    string subbags = line.substr(keysplit + 13);
    children.clear();
    if (subbags != "no other bags.") {

      // break into subbags
      vector<string> bags;
      string bagstring;
      istringstream tokenStream(subbags);
      bool first = true;
      while (getline(tokenStream, bagstring, ',')) {
        if (first) {
          bags.push_back(bagstring);
          first = false;
        } else {
          bags.push_back(bagstring.substr(1)); // delete ','
        }
      }
      bags.back().pop_back(); // delete '.' from last bagline

      // fill children list
      for (auto bag : bags) {
        int amountsplit = bag.find(" ");
        string name = bag.substr(amountsplit + 1, bag.find("bag") - 3);
        int amount = stoi(bag.substr(0, amountsplit));
        children.insert({name, amount});
      }
    }

    baglist.insert({key, children});
  }

  int valid = 0;
  int valid2 = 0;
  for (auto bag : baglist) {
    // if (checkContain("shiny gold", bag.first, baglist))
    //   valid++;
    if (bag.first == "shiny gold")
      valid2 = sumContained("shiny gold", baglist);
  }

  cout << "Part One: " << valid << endl;
  cout << "Part Two: " << valid2 << endl;

  cout << endl;
}
