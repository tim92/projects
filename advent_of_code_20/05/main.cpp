#include <math.h>
#include <fstream>
#include <bitset>
#include <iostream>
#include <string>
#include <vector>

using namespace std;


int main() {
    string tmp;
    ifstream f("input");
    vector<string> passes;
    vector<bool> seats (1000);

    while (getline(f, tmp)) {
        passes.push_back(tmp);
    }

    int valid = 0;
    int valid2 = 0;
    for (auto pass : passes) {
        int row = 0;
        int start_row = 128;
        for (auto ch : pass.substr(0, 7)) {
            start_row /= 2;
            if (ch == 'B')
                row += start_row;
        }
        int col = 0;
        int start_col = 8;
        for (auto ch : pass.substr(7)) {
            start_col /= 2;
            if (ch == 'R')
                col += start_col;
        }
        int id = row * 8 + col;
        valid = max(valid, id);
        seats.at(id) = true;
        //cout << id << "\t" << pass << "\n";
    }
    for (size_t i = 0; i < seats.size(); i++) {
        // 45 to 953 are usable
        if (i >= 45 && i <= 953 && seats.at(i) == false)
            valid2 = i;
    }

    cout << "Part One: " << valid << "\n";
    cout << "Part Two: " << valid2 << "\n";

    cout << "\n";
}
