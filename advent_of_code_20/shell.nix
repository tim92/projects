with import <nixpkgs> { };

stdenv.mkDerivation rec {
  name = "advent20";
  env = buildEnv {name = name; paths = buildInputs; };
  buildInputs = [
    gcc
    gnumake
    inotify-tools
  ];
  shellHook = ''
inotifywait -e close_write -m * |
while read -r directory events filename; do
  if [ "$filename" = "main.cpp" ]; then
    pushd $directory >> /dev/null
    make
    popd >> /dev/null
  fi
done
  '';
}
