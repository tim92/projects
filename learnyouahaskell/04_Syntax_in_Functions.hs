-- Pattern Matching:
--  * Define separate function bodies for different patterns/forms of input
--  * Match from top to bottom
--  * Can also pattern match in lists
--  * And lists can be used in pattern matching
--  * Use 'as pattern' when you need to reuse the entire match in the function
--    body with '<as pattern>@<pattern>'
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)

-- Guards:
--  * Match on properties of input instead of form
--  * Where keyword for reusing parts (also functions), nesting possible
max' :: (Ord a) => a -> a -> a
max' a b
  | a > b = a
  | otherwise = b
  where
    c = a > b

-- Let:
--  * let <bindings> in <expression>, make code smaller/more readable
--  * Expressions, where bindings are just syntatic constructs
--  * Can also be combined with predicates
--  * Can not be used in guards as opposed to where because of scope
--    restrictions
calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi | (w, h) <- xs, let bmi = w / h ^ 2, bmi >= 25.0]

-- Case expressions:
--  * Syntactic sugar for pattern matching
describeList :: [a] -> String
describeList xs =
  "The list is " ++
  case xs of
    [] -> "empty."
    [x] -> "a singleton list."
    xs -> "a longer list."

