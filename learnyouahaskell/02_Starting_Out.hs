-- Starting Out

-- Simple arithmetic and boolean algebra works just like anywhere else:
--  * All operators are infix functions.
--  * Put negative numbers in braces.
--  * Unequality with '/='.

-- Functions:
--  * Function application has the highest precedence.
--  * Write infix functions with '`', eg: '10 `div` 4'.

doubleMe x = x + x
doubleUs x y = doubleMe x + doubleMe y
doubleSmallNumber x = if x < 100 then doubleMe x else x

-- Lists: '[e1, ..., en]'
--  * Homogenous, also nested ones.
--  * Concatenate with '++', has to iterate over left list, both arguments are
--    lists.
--  * Prepend element (cons) with ':', instant, arguments are element and list.
--  * '[1,2,3]' is syntactic sugar for '1:2:3:[]'
--  * Get element by index with '!!'.
--  * Compared in lexicographical order.
--  * Basic functions (careful: always check for empyt list):
--    * head: take first element
--    * tail: take elements without first
--    * last: take last element
--    * init: take elements without last
--    * length: length of list
--    * null: whether list is empty
--    * reverse: reverse list
--    * take n: take n first elements
--    * drop n: drop n last elements
--    * maximum/minimum
--    * sum/product
--    * n `elem` xs: is n element of xs

-- Ranges: '[x1 x2 .. [limit]]'
--  * works with numbers and characters (lower- and -- uppercase). No limit has
--    to be provided since haskell is lazy.
--  * Do not use floating point numbers in ranges!
--  * Basic functions:
--    * cycle xs: create infinite list from xs
--    * repeat n: create infinite list from n
--    * replicate n1 n2: create n1 occurences of n2

-- List Comprehensions: '[output function | input set(s), predicate]'
--  * When drawing from multiple sets, the cross product is created
--  * Create anonymous parameters like this:
length' xs = sum [1 | _ <- xs] 
--  * Strings are lists and can thus be processed and produced by list
--    comprehensions. Nested list comprehensions are also possible.

-- Tuples: '(e1, e2)'
--  * Type depends on number of components and those types, not homogenous
--  * Can contain lists
--  * basic functions:
--    * fst: take first element
--    * snd: take second element
--    * zip: make touple list out of two lists, longer one gets cut off
pyth = [ (a, b, c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2, a+b+c == 24]
 
