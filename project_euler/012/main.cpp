#include <string>
#include <iostream>

using namespace std;

int main () {
	int teiler = 0, triangNum = 0;
	for (int i = 1; i < 1000 ; ++i)
	{
		triangNum += i;
		cout << triangNum;
		for (int j = 1; j <= triangNum; ++j)
		{
			if ((triangNum % j) == 0)
			{
				teiler++;
			}
		}
		cout << " " << teiler << "\n";
		if (teiler > 32)
		{
			break;
		}
		teiler = 0;
	}
	return 0;
}
