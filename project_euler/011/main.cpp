#include <string>
#include <iostream>

using namespace std;

int main () {
	int grid[20][20];
	string rows[20];
	rows[0] = "0802229738150040007504050778521250779108";
	rows[1] = "4949994017811857608717409843694804566200";
	rows[2] = "8149317355791429937140675388300349133665";
	rows[3] = "5270952304601142692468560132567137023691";
	rows[4] = "2231167151676389419236542240402866331380";
	rows[5] = "2447326099034502447533537836842035171250";
	rows[6] = "3298812864236710263840675954706618386470";
	rows[7] = "6726206802621220956394396308409166499421";
	rows[8] = "2455580566739926971778789683148834896372";
	rows[9] = "2136230975007644204535140061339734313395";
	rows[10] = "7817532822753167159403800462161409535692";
	rows[11] = "1639054296353147555888240017542436298557";
	rows[12] = "8656004835718907054444374460215851541758";
	rows[13] = "1980816805944769287392138652177704895540";
	rows[14] = "0452088397359916079757321626267933279866";
	rows[15] = "8836688757622072034633674655123263935369";
	rows[16] = "0442167338253911249472180846293240627636";
	rows[17] = "2069364172302388346299698267598574043616";
	rows[18] = "2073352978319001743149714886811623570554";
	rows[19] = "0170547183515469169233486143520189196748";
	// grid einlesen
	for (int i = 0; i < 20; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			grid[i][j] = 10 * (rows[i].at(2 * j) - 48) + (rows[i].at((2 * j) + 1) - 48);
		}
	}
	long int grProd = 0, tester = 0;
	for (int i = 0; i < 20; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			// hoch und runter, nach unten beschraenkt
			if (i < 17)
			{
				tester = grid[i][j] * grid[i+1][j] * grid[i+2][j] * grid[i+3][j];
				if (tester > grProd)
				{
					grProd = tester;
				}
			}
			// links und rechts, nach rechts beschraenkt
			if (j < 17)
			{
				tester = grid[i][j] * grid[i][j+1] * grid[i][j+2] * grid[i][j+3];
				if (tester > grProd)
				{
					grProd = tester;
				}
			}
			// diagonal rechts unten beschr
			if ((i < 17) && (j < 17))
			{
				tester = grid[i][j] * grid[i+1][j+1] * grid[i+2][j+2] * grid[i+3][j+3];
				if (tester > grProd)
				{
					grProd = tester;
				}
			}
			// diagonal links unten beschr
			if ((i < 17) && (j > 2))
			{
				tester = grid[i][j] * grid[i+1][j-1] * grid[i+2][j-2] * grid[i+3][j-3];
				if (tester > grProd)
				{
					grProd = tester;
				}
			}
		}
	}
	cout << grProd;
	return 0;
}
