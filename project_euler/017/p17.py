# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 13:30:05 2017

@author: Tim
"""

to_twenty = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
             "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
             "eighteen", "nineteen", "twenty"]
ones = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
tens = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
hundred = "hundred"
thousand = "one thousand"

lens = []

for i in range(1, 1001):
    s = ""
    if i <= 20:
        s = to_twenty[i-1]
    if 20 < i < 100:
        if not (i%10):
            s = tens[(i//10)-2]
        else:
            s = tens[(i//10)-2] + "-" + ones[(i%10)-1]
    if 100 <= i < 1000:
        if not (i%100):
            s = ones[(i//100)-1] + " " + hundred
        elif (i%100) < 20:
            s = ones[(i//100)-1] + " " + hundred + " and " + to_twenty[(i%100)-1]
        elif not (i%10):
            s = ones[(i//100)-1] + " " + hundred + " and " + tens[((i%100)//10)-2]
        else:
            s = ones[(i//100)-1] + " " + hundred + " and " + tens[((i%100)//10)-2] + "-" + ones[(i%10)-1]
    if i == 1000:
        s = thousand
    l = len(s.replace(' ', '').replace('-', ''))
    lens.append(l)

print(sum(lens))
