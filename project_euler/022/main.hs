import Control.Monad
import Data.Char
import Data.List
import System.IO  

-- | Calculate the alphabetical value of a string ('a'=1, 'b'=2, ..., summed up)
nameToNum :: [Char] -> Int
nameToNum s = sum [ ord (toLower c) - 96 | c <- s ]

main = do
    contents <- readFile "p022_names.txt"   -- Read namelist
    let wordlist = sort (words $ contents)  -- Sort names
    -- Sum up the alph. value multiplied by the name's position of all names
    let s = sum [ fst e * snd e
                | e <- zip [ nameToNum w | w <- wordlist ]
                           [1..(length wordlist)] ]
    print s

