#include <iostream>

using namespace std;

class number {
	public:
    int digits[6];
    void set_values (int);
    bool is_pal ();
};

void number::set_values (int x) {
  	digits[5] = x % 10;
  	digits[4] = (x % 100) / 10;
  	digits[3] = (x % 1000) / 100;
  	digits[2] = (x % 10000) / 1000;
  	digits[1] = (x % 100000) / 10000;
  	digits[0] = x / 100000;
}

bool number::is_pal () {
	if (digits[5] == digits[0] && digits[4] == digits[1] && digits[3] == digits[2])
	{
		return true;
	}
	return false;
}

int main () {
  number num1;
  for (int i = 999; i > 900; i--)
  {
  	for (int j = 999; j > 900; j--)
  	{
  		num1.set_values (i*j);
  		if (num1.is_pal())
  		{
  			cout << "\ngefunden\n" << i*j << " i:" << i << " j:" << j;
  		}
  	}
  }
  return 0;
}
