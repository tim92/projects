#include <iostream>

using namespace std;

int main () {
	for (int i = 20; i < 1000000000; i += 20)
	{
		for (int j = 1; j < 21; j++)
		{
			if ((i % j) != 0)
			{
				break;
			}
			if (j == 20)
			{
				cout << i;
				return 0;
			}
		}
	}
	return 0;
}
