#include <iostream>

using namespace std;

int main() {
	int sum = 0, fib1 = 1, fib2 = 2, fib3 = 0;
	while(fib1 < 4000000) {
		if (fib1 % 2 == 0)
		{
			sum += fib1;
			cout << fib1 << "  sum:" << sum << "  ";
		}
		fib3 = fib2 + fib1;
		fib1 = fib2;
		fib2 = fib3;
	}
	cout << sum << "\n";
}
