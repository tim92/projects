#include <iostream>

using namespace std;

int main() {
	long long n = 600851475143, teiler = 2;
	while (n != 1) {
		if ((n % teiler) == 0)
		{
			cout << teiler << "  ";
			n = n/teiler;
		}
		teiler++;
	}
}
