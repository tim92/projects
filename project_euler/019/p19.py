# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 15:45:40 2017

@author: Tim
"""

import datetime
count = 0
for y in range(1901,2001):
    for m in range(1,13):
        if datetime.datetime(y,m,1).weekday() == 6:
            count += 1
print(count)