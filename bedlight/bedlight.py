#!/usr/bin/env python3
from apa102_pi.driver import apa102
from flask import Flask, request, send_from_directory
from multiprocessing import Process, Value
import os
import time
import RPi.GPIO as GPIO


app = Flask(__name__)
button_pin = 14
color = '#6e5300'
initial_color = color
leds = 96
state = 0
button_presses = Value('i', 0)
bus_speed = 100000
strip = apa102.APA102(num_led=leds,bus_speed_hz=bus_speed)
strip.clear_strip()

def button_action():
    global state, color
    color = '#000000' if state else initial_color 
    state = state ^ 1  # XOR state with 1
    print('state', state, 'color', color)
    set_color(color)

def button_presses_inc(button_presses):
    button_presses.value += 1
    print('pressed', button_presses.value, 'times')
    time.sleep(3)
    button_presses.value -= 1

def button_callback(button_presses):
    if 3 <= button_presses.value:
        button_action()
    else:
        Process(target=button_presses_inc, args=(button_presses,)).start()

GPIO.setup(button_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(button_pin, edge=GPIO.RISING, callback=lambda x:
        button_callback(button_presses), bouncetime=500)

def set_color(color):
    for led in range(0, leds):
        strip.set_pixel_rgb(led, int(color[1:], base=16))
    strip.show()

@app.route('/', methods = ['GET'])
def root():
    return send_from_directory(os.getcwd(), 'index.html')

@app.route('/on', methods = ['POST'])
def on():
    global state, color
    color = request.get_data().decode('utf-8')
    state = 1
    print('state', state, 'color', color)
    set_color(color)
    return '', 202

@app.route('/off', methods = ['POST'])
def off():
    global state
    state = 0
    print('state', state, 'color', color)
    set_color('#000000')
    return '', 202

if __name__ == '__main__':
    app.run()
