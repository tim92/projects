# Pi Setup

## SD card preparation

### Download image

Download the latest image
[here](https://www.raspberrypi.org/downloads/raspbian/).

### Verify downloaded image

To verify the download, create a file called `<IMG>.img.sha256` with `<HASH
VALUE COPIED FROM WEBSITE> <IMG>.img`, verify with `sha256sum -c
<IMG>.img.sha256`.

### Flash to SD card

Flash the image to the SD card `dd bs=4M if=<IMG>.img of=/dev/sdX conv=fsync`

### Start SSH on first boot

Mount the first partition of the SD card `sudo mount /dev/sdX1 tmp`, `cd tmp`
and `sudo touch ssh` so the SSH service will be started on first boot.

## Libs setup

Covered by `install.sh`

## Hardware setup

Wire everything according to the Fritzing schema and enable SPI with `raspi-config`

## Software setup

First SPI and LED test: run `sample.py` in the `APA102_pi` folder.

### Python

The server can be setup with

```
mod_wsgi-express setup-server wsgi.py --port=80 --user=pi --group=pi --server-root=./server_root/
```

and started with the systemd unit or

```
flask run --host 0.0.0.0
```

implement fireplace with multiprocessing
`https://docs.python.org/3/library/multiprocessing.html#module-multiprocessing`
