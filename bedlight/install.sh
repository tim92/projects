#!/usr/bin/env bash

sudo apt install python3 python3-pip python3-dev python3-smbus python3-rpi.gpio -y
sudo python3 -m pip install --upgrade pip setuptools wheel
git clone https://github.com/adafruit/Adafruit_Python_GPIO.git ~/adafruit_GPIO
pushd ~/adafruit_GPIO
sudo python3 setup.py install
popd
git clone https://github.com/tinue/APA102_Pi.git ~/apa102
pushd ~/apa102
sudo pip3 install .
popd
sudo python3 -m pip install mod_wsgi-standalone
sudo python3 -m pip install flask
sudo cp ~/projects/bedlight/bedlight.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable bedlight.service
sudo systemctl start bedlight.service
