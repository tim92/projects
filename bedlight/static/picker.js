var xhttp = new XMLHttpRequest();
var divWidth = document.getElementById('picker').clientWidth * 0.99;
var col = '#6e5300';
var colorPicker = new iro.ColorPicker('#picker', {
    width: divWidth,
    color: col
});

function setColor(col) {
    document.body.style.background = col;
    document.getElementById('button').style.borderColor = col;
    document.getElementById('button').style.color = col;
}

// set initial color
document.onload = setColor(col);

// input:end or input:change
colorPicker.on('input:change', function(color) {
    console.log(color.hexString);
    setColor(color.hexString);
    xhttp.open('POST', '/on', true);
    xhttp.setRequestHeader('Content-type', 'text/plain');
    xhttp.send(color.hexString);
});

// turn off
function off() {
    console.log('off');
    xhttp.open('POST', '/off', true);
    xhttp.send();
};
