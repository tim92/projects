#include "stdio.h"

#define MAX_WORD_LENGTH 20  /* maximum length of words */
#define IN  1   /* inside a word    */
#define OUT 0   /* outside a word   */

int main()
{
    int c, wl, count[MAX_WORD_LENGTH], state;

    for (int i = 0; i < MAX_WORD_LENGTH; ++i)
        count[i] = 0;

    wl = 0;
    state = OUT;

    while ((c = getchar()) != EOF) {
        if (c == ' ' || c == '\t' || c == '\n') {
            if (state) {
                ++count[wl];
                wl = 0;
            }
            state = OUT;
        } else {
            ++wl;
            state = IN;
        }
    }

    printf("length | occurences\n");
    for (int i = 0; i < MAX_WORD_LENGTH; ++i) {
        printf("%6d | ", i);
        for (int j = 0; j < count[i]; ++j)
            printf("+");
        printf("\n");
    }
}

