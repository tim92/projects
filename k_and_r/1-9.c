#include "stdio.h"
#include "stdbool.h"

int main()
{
    int c;
    bool is_blank = 0;

    while ((c = getchar()) != EOF) {
        if (c != ' ') {
            is_blank = 0;
            putchar(c);
        } else {
            if (!is_blank) {
                is_blank = 1;
                putchar(c);
            }
        }
    }
}

