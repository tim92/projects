#include "stdio.h"

#define NUM_CHARS 29    /* only count lowercase letters and whitespace */
#define CHAR_OFFSET 97  /* offset in ASCII table */

int main()
{
    int c, count[NUM_CHARS];

    for (int i = 0; i < NUM_CHARS; ++i)
        count[i] = 0;

    while ((c = getchar()) != EOF) {
        if (c == ' ')
            ++count[26];
        if (c == '\t')
            ++count[27];
        if (c == '\n')
            ++count[28];
        else
            ++count[c - CHAR_OFFSET];
    }

    printf("char    | occurences\n");
    for (int i = 0; i < NUM_CHARS; ++i) {
        if (i == 26)
            printf("space   | ");
        if (i == 27)
            printf("tab     | ");
        if (i == 28)
            printf("newline | ");
        if (i < 26)
            printf("%c       | ", i + CHAR_OFFSET);
        for (int j = 0; j < count[i]; ++j)
            printf("+");
        printf("\n");
    }
}

