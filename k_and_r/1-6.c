#include "stdio.h"

int main()
{
    char c;

    while ((c = getchar()) != EOF) {
        printf("EOF: %d, char: %d\n", c != EOF, c);
    }
    printf("EOF: %d\n", c != EOF);
}

