#include "stdio.h"

#define MINLEN 50 /* minimum line length for a line to be printed (pos int) */
#define NO 0  /* do not print newline */
#define YES 1 /* do print newline */

int main()
{
    int i, c, printn;
    char line[MINLEN];

    printn = NO;
    i = 0;
    while ((c = getchar()) != EOF) {
        /* reset counter on newline and print newline if wanted */
        if (c == '\n') {
            i = 0;
            if (printn)
                putchar(c);
                printn = NO;
        /* within a line */
        } else {
            /* save input to buffer if candidate for printing */
            if (i < (MINLEN - 1))
                line[i] = c;
            /* print buffer and newline later on */
            if (i == (MINLEN - 1)) {
                line[i] = '\0';
                printf("%s", line);
                putchar(c);
                printn = YES;
            }
            /* print rest of line */
            if (i >= MINLEN)
                putchar(c);
            ++i;
        }
    }
    return 0;
}

