#include "stdio.h"

#define TAB_STOPS 8 /* number of characters per tab stop */

int main()
{
    int i, c;

    i = TAB_STOPS;
    while ((c = getchar()) != EOF) {
        /* insert spaces on tab and reset counter */
        if (c == '\t') {
            for (int j = 0; j < i; ++j)
                putchar(' ');
            i = TAB_STOPS;
        } else {
            /* print other chars */
            putchar(c);
            /* reset tab stop counter on newline */
            if (c == '\n')
                i = TAB_STOPS;
        }
        /* reset tab stop counter after TAB_STOPS columns */
        if (i == 0)
            i = TAB_STOPS;
        else
            --i;
    }
    return 0;
}

