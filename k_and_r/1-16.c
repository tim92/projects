#include "stdio.h"

#define MAXLINE 50 /* max input line length */

int getline2(char line[], int maxline);
void copy(char to[], char from[], int maxline);

int main()
{
    int len;    /* current line length */
    int max;    /* max line length */
    char line[MAXLINE];     /* current line */
    char longest[MAXLINE];  /* longest line */

    for (int i = 0; i < MAXLINE; ++i) {
        line[i] = ' ';
        longest[i] = ' ';
    }
    max = 0;
    while ((len = getline2(line, MAXLINE)) > 0) {
        if (len > max) {
            max = len;
            copy(longest, line, MAXLINE);
        }
    }
    if (max > 0)    /* there was a line */
        printf("first %d characters of longest line: %s", MAXLINE, longest);
    return 0;
}

/* read line, return length */
int getline2(char line[], int maxline)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i)
        if (i < (maxline - 1))
            line[i] = c;
    if (i == maxline) {
        if (c == '\n') {
            line[i] = c;
            ++i;
        }
        line[i] = '\0';
    } else
        line[maxline - 1] = '\0';
    return i;
}

void copy(char to[], char from[], int maxline)
{
    int i;

    i = 0;
    while ((to[i] = from[i]) != '\0' && i < (maxline - 1))
        i++;
}

