with import <nixpkgs>{};

stdenv.mkDerivation rec {
	name = "mtg";
	env = buildEnv {name = name; paths = buildInputs; };
	buildInputs = [
		python3
		python3Packages.pillow
	];
}
