#! /usr/bin/env nix-shell
#! nix-shell -i python -p python3 python3Packages.pillow

import os
import time
import json
from PIL import Image
from urllib import request, parse


pdfs = 0
front_pages = []
back_pages = []

for dirpath in [el for el in os.listdir('.') if os.path.isdir(el)]:
    print('\n===== downloading images for ' + dirpath)

    with open(dirpath + '/deck.txt') as dl:
        decklist = dl.readlines()

    print('=== maindeck')
    for line in decklist:
        if '\n' == line:
            print('=== sideboard')
            continue

        name = ' '.join(line.strip().split()[1:])
        quantity = int(' '.join(line.strip().split()[0]))
        if name in ['Swamp', 'Forest', 'Plains', 'Island', 'Mountain']:
            print('= skipping standard land ' + name)
            continue

        basename = dirpath + '/' + name.replace(' ', '_')
        imagename = basename + '.png'
        if os.path.isfile(imagename):
            pause = 0
            print('= skipping already existing image ' + name)
        else:
            pause = 0.1
            print('= downloading ' + name)
            url = 'https://api.scryfall.com/cards/named?exact=' + parse.quote(name)
            json_response = json.loads(request.urlopen(url).read())
            if 'card_faces' in json_response.keys():
                uri = json_response['card_faces'][0]['image_uris']['png']
                uri_back = json_response['card_faces'][1]['image_uris']['png']
                with open(basename + '_back.png', 'wb') as f:
                    f.write(request.urlopen(uri_back).read())

            else:
                uri = json_response['image_uris']['png']

            with open(imagename, 'wb') as f:
                f.write(request.urlopen(uri).read())

        x = 750
        y = 1050
        offset = 36

        front = Image.open(imagename).resize((x, y))
        front_base = Image.new('RGBA', (x + 2 * offset, y + 2 * offset), (0, 0, 0, 255))
        front_rgb = Image.new('RGB', (x + 2 * offset, y + 2 * offset), (0, 0, 0))
        front_base.paste(front, box=(offset, offset))
        front_rgb.paste(front_base, mask=front_base.split()[3])

        if os.path.isfile(basename + '_back.png'):
            back = Image.open(basename + '_back.png').resize((x, y))
        else:
            back = Image.open('back.png').resize((x, y))
        back_base = Image.new('RGBA', (x + 2 * offset, y + 2 * offset), (0, 0, 0, 255))
        back_rgb = Image.new('RGB', (x + 2 * offset, y + 2 * offset), (0, 0, 0))
        back_base.paste(back, box=(offset, offset))
        back_rgb.paste(back_base, mask=back_base.split()[3])

        for _ in range(quantity):
            if len(front_pages) == 54:
                front_pages[0].save('front_' + str(pdfs) + '.pdf', save_all=True,
                    append_images=front_pages[1:] + [front_rgb], resolution=300)
                front_pages = []
                back_pages[0].save('back_' + str(pdfs) + '.pdf', save_all=True,
                    append_images=back_pages[1:] + [back_rgb], resolution=300)
                back_pages = []
                pdfs += 1
            else:
                front_pages.append(front_rgb)
                back_pages.append(back_rgb)

        time.sleep(pause)

front_pages[0].save('front_' + str(pdfs) + '.pdf', save_all=True,
    append_images=front_pages[1:] + [front_rgb], resolution=300)
back_pages[0].save('back_' + str(pdfs) + '.pdf', save_all=True,
    append_images=back_pages[1:] + [back_rgb], resolution=300)
